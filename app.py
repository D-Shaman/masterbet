"""
Fichier app.py de l'application Masterbet,
Contient l'instanciation de l'application et les routes.
"""
import os
import datetime
from datetime import datetime
from flask import Flask, request, render_template, redirect, url_for, session, send_from_directory
from werkzeug.utils import send_from_directory
from controller.commentController import CommentController
from controller.postController import PostController
from controller.categoryController import CategoryController
from controller.prediOptionController import PrediOptionController
from controller.userController import UserController
from controller.groupController import GroupController
from model.commentModel import CommentModel
from model.postModel import PostModel
from model.categoryModel import CategoryModel
from model.prediOptionModel import PrediOptionModel
from model.userModel import UserModel
from model.groupModel import GroupModel


app = Flask(__name__, static_folder='templates/assets')
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = os.urandom(24)

postController = PostController()
postModel = PostModel()
commentController = CommentController()
commentModel = CommentModel()
categoryController = CategoryController()
categoryModel = CategoryModel()
prediOptionController = PrediOptionController()
prediOptionModel = PrediOptionModel()
userController = UserController()
userModel = UserModel()
groupController = GroupController()
groupModel = GroupModel()

            #######################
            #                     #
            #    Generic pages    #
            #                     #
            #######################

@app.route("/", methods=['POST','GET'])
def home():
    """
    Route vers la page d'accueil
    """
    categories = categoryModel.get_categories()
    return postController.fetch_all_posts(postModel, categories)

@app.route("/login")
def login():
    """
    Route vers la page de connexion
    """
    return render_template('login.html')

@app.route("/register_page")
def register_page():
    """
    Route vers la page d'inscription
    """
    return render_template('register_page.html')

@app.route("/logout")
def logout():
    """
    Route et logique permettant a l'utilisateur de se déconnecter,
    ie. de flush les informations de session.
    """
    session.clear()
    return redirect(url_for('home'))

@app.route("/resolved_posts", methods = ['POST', 'GET'])
def get_all_post_resolved():
    """
    Méthode et logique permettant de voir les post résolu
    """
    categories = categoryModel.get_categories()
    return postController.fetch_all_posts_resolved(postModel, categories)


@app.route("/dashboard")
def dashboard():
    """Route affichant le tableau de bord personnel d'un utilisateur connecté"""
    try:
        user_id = session['user_id']
        name = session['user_name']
        wallet = session['user_wallet']
        total_masters = userController.get_total_masters_played_by_user(userModel, user_id)
        groups = groupController.show_user_groups(groupModel, user_id)
        return postController.get_dashboard(postModel, user_id, name, wallet, total_masters, groups)
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {dashboard.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Il y a eu une erreur... Vérifiez votre statut de connexion")


            ########################
            #                      #
            #  Register / Sign in  #
            #                      #
            ########################

@app.route("/user_register", methods=['GET', 'POST'])
def user_register():
    """
    Logique permettant d'enregistrer les informations d'un nouvel utilisateur en base de donnée
    """
    form_data = request.form
    return userController.user_register(userModel, form_data)


@app.route("/user_sign_in", methods=['GET', 'POST'])
def user_sign_in():
    """
    Logique permettant à un utilisateur de se connecter et de démarrer une session
    Si l'utilisateur n'est pas en db, user_data sera false, donc erreur.
    Si l'utilisateur existe en db , alors on fetch les information de connection.
    """
    form_data = request.form
    user_data = userController.user_login(userModel, form_data)
    if user_data:
        try:
            session['user_id'] = user_data[0][0]
            session['user_name'] = user_data[0][1]
            session['user_last_name'] = user_data[0][2]
            session['user_wallet'] = int(user_data[0][3])
            session['logged_in']= True
            return redirect(url_for('home'))
        except (Exception) as error :
            with open("errorlogapp.txt", "a") as errorlog:
                errorlog.write(
                    f"Where: {user_sign_in.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
            return render_template('error.html', error="Erreur d'authentification !")
    else:
        return render_template('error.html', error="Erreur d'authentification !")


            #######################
            #                     #
            #        Posts        #
            #                     #
            #######################


@app.route("/post/<post_id>", methods=['GET', 'POST'])
def show_post(post_id):
    """
    Si l'utilisateur est connecté, logique permetant d'afficher un pari en particulier.
    La logique permet de définir si l'utilisateur est en mesure de jouer.
    """
    try:
        if session:
            user_id = session['user_id']
            played = userController.get_played_options(userModel, user_id)
            bet_option = prediOptionController.get_options_id(prediOptionModel, post_id)
            played_id_list = []
            bet_option_list = []
            # Récupère les ID des options
            # Si on retrouve l'ID d'une des options de ce pari
            # Alors l'utilisateur ne peut plus jouer
            for played_option_id in played:
                played_id_list.append(played_option_id[0])
            for bet_option_id in bet_option:
                bet_option_list.append(bet_option_id[0])
            check = any(item in played_id_list for item in bet_option_list)
        else:
            check = True
        return postController.show_post(postModel, post_id, check)
    except(Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {create_post.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Ce pari n'existe pas (ou plus !)")

@app.route("/create_post", methods = ['POST', 'GET'])
def create_post():
    """
    Logique permettant a un utilisateur de créer un post. Enregistrement des informations en base de donnée
    """
    try:
        user_id = session['user_id']
        form_data = request.form
        postController.create_post(postModel, form_data, user_id)
        post_id = prediOptionController.get_post_id(postModel, user_id, form_data)
        prediOptionController.add_options(prediOptionModel, form_data, post_id)
        return redirect(url_for('show_post', post_id=post_id))
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {create_post.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Il y a eu une erreur. Vérifiez votre statut de connexion")


@app.route("/post_form")
def post_form():
    """
    Route permettant l'affichage du formulaire de création de pari
    """
    try:
        user_id = session['user_id']
        categories = categoryController.show_categories(categoryModel)
        user_groups = userController.get_user_groups(userModel, user_id)
        return render_template('post_form.html', categories=categories, user_groups=user_groups)
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {post_form.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Vous devez être connecté pour pouvoir poster !")


@app.route("/set_winner/<int:post_id>/<int:option_id>")
def set_winner(post_id, option_id):
    """
    Logique permettant de déterminer les gagnants d'un pari résolu
    Logique permettant l'attribution des gains aux gagnants
    """
    try:
        prediOptionController.make_winner(prediOptionModel, option_id)
        id_and_masters_played_winners = prediOptionController.get_winners_id_and_masters_played(prediOptionModel, option_id)
        total_masters = prediOptionController.get_total_masters(prediOptionModel, post_id)
        prediOptionController.determine_and_attribute_winnings(prediOptionModel, id_and_masters_played_winners, total_masters)
        postController.update_post_state(postModel, post_id)
        session['user_wallet'] = int(userController.get_user_wallet(userModel, session['user_id']))
        return redirect(url_for('home'))
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {set_winner.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
            return render_template('error.html', error="Il y a eu une erreur. Si elle persiste, réferrez-vous à un administrateur")


@app.route('/search_posts', methods = ['POST'])
def search_posts():
    """
    Logique permettant d'efectuer une recherche de pari par son titre
    et de les afficher dans une vue
    """
    form_data = request.form
    data = form_data.get('search')
    posts = postController.search_posts(postModel, data)
    categories = categoryModel.get_categories()
    return render_template('home2.html', paris=posts, categories=categories)


            #######################
            #                     #
            #       Comments      #
            #                     #
            #######################

@app.route("/create_comment/<post_id>", methods = ['POST', 'GET'])
def create_comment(post_id):
    """
    Logique permettant a l'utilisateur de créer un commentaire
    Ajout des informations en base de donnée
    """
    try:
        form_data = request.form
        user_id = session['user_id']
        commentController.create_comment(commentModel, form_data, user_id)
        return redirect(url_for('show_post', post_id=post_id))
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {create_comment.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Il y a eu une erreur... Vérifiez votre statut de connexion")

@app.route("/delete_comment/<post_id>/<comment_id>", methods=['POST', 'GET', 'DELETE'])
def delete_comment(comment_id, post_id):
    """
    Logique permettant la suppression d'un commentaire par son auteur
    """
    commentController.delete_comment(commentModel, comment_id)
    return redirect(url_for("show_post", post_id=post_id))


            #######################
            #                     #
            #      Categories     #
            #                     #
            #######################

@app.route("/category/<id_category>", methods = ['GET', 'POST'])
def posts_per_categories(id_category):
    """
    Logique permettant a l'utilisateur d'afficher les posts selon une catégorie spécifique
    """
    print(type(id_category))
    try:
        categories = categoryModel.get_categories()
        return postController.fetch_posts_per_categories(postModel, categoryModel, id_category, categories)
    except (Exception) as error :
        with open("errorlogapp.txt", "a") as errorlog:
            errorlog.write(
                f"Where: {posts_per_categories.__name__}\nDate : {datetime.utcnow()}, \n    Error : Unexpected {error=}, {type(error)=} \n------\n")
        return render_template('error.html', error="Cette catégorie n'existe pas (ou plus !)")


            #######################
            #                     #
            #         Play        #
            #                     #
            #######################


@app.route("/play_bet/", methods=['POST','GET'])
def play_bet():
    """
    Logique permettant à un utilisateur de jouer sur un pari:
    Elle vérifie si les conditions (déjà joué, assez de masters, pari ouvert)
    sont respectées et retire les masters joués à l'utilisateur
    """
    form_data = request.form
    user_id = session['user_id']
    playing_conditions = userController.playing_conditions(form_data, userModel, user_id)
    if playing_conditions:
        played_masters = int(form_data.get('masters'))
        session['user_wallet'] -= played_masters
        return userController.play(form_data, userModel, user_id)
    return render_template('error.html', error="Pas assez de masters !")


            #######################
            #                     #
            #       Groups        #
            #                     #
            #######################

@app.route("/group_form")
def group_form():
    """
    Route du pemplate permettant a l'utilisateur d'accéder au formulaire de création d'un groupe
    """
    return render_template('group_form.html')

@app.route("/create_group", methods=['POST','GET'])
def create_group():
    """
    Cette fonction prend les information du formulaire,
    puis va appeller une méthode du groupController qui va créer le groupe
    puis va appeller une méthode du groupController qui va ajouter les membres aux groupe
    """
    form_data = request.form
    creator_id = session['user_id']
    group_name = form_data.get('name')
    groupController.create_group(groupModel, group_name)
    group_id = groupController.get_group_id(groupModel, group_name)
    list_id = groupController.get_ids_from_email(groupModel, form_data)
    print("***********")
    print(f"ID du groupe: {group_id}")
    print("***********")
    print(f"Liste des ID des personnes a ajouter au groupe {list_id}")
    print("***********")
    groupController.add_group_creator(groupModel, creator_id, group_id)
    if list_id:
        print("Ajout des membres")
        groupController.add_group_members(groupModel, group_id, list_id)
    else:
        print("Liste vide")
    return redirect('/')

@app.route("/group/<int:group_id>")
def fetch_posts_per_group(group_id):
    """
    Logique et route permettant a l'utilisateur d'afficher les posts spécifique a un groupe
    """
    return postController.fetch_posts_per_group(postModel, group_id)

@app.route("/quit_group/<int:group_id>")
def quit_group(group_id):
    """
    Logique et route permettant à l'utilisateur de quitter le groupe selectionné
    """
    user_id = session['user_id']
    groupController.quit_group(groupModel, group_id, user_id)
    return redirect(url_for('dashboard'))

            #######################
            #                     #
            #       Robots        #
            #                     #
            #######################

@app.route("/robots.txt")
def static_for_robots():
    path="robots.txt"
    return send_from_directory(app.static_folder, path, request.environ)