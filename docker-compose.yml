version: "3.7"

networks:
  sonar:
  masterbet-net:
    driver: bridge
    ipam:
      driver: default
      config:
      - subnet: 172.20.0.0/16
        gateway: 172.20.0.1

volumes:
  mysqldump:
  mysqldb:
  sonarqube_conf:
  sonarqube_data:
  sonarqube_extensions:
  sonar_postgresql:
  sonar_postgresql_data:
  elasticsearch:

services:

  masterbet:
    image: dlefebvre/masterbet:mysql_1.3.3
    # build:
    #   dockerfile: Dockerfile
    #   context: ./
    ports: #Le port est a retiré quand nginx est lancé, ou au pire faudrait changer de port
      - "5000:5000"
    container_name: masterbet_app
    restart: unless-stopped
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.10
    depends_on:
      - mysql
    profiles:
      - min
      - elk
      - tig
      - full
      - runner_app
  
  mysql:
    image: mysql:8
    container_name: mysql
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: "root"
    volumes:
      - "./mysqldump/dump_masterbet_final.sql:/docker-entrypoint-initdb.d/dump.sql"
    ports:
      - 3310:3306
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.15
    profiles:
      - min
      - elk
      - tig
      - full
      - runner_app
    

  sonarqube:
    image: sonarqube:7.5-community
    container_name: sonarqube
    restart: unless-stopped
    ports:
      - "7000:9000" #Run sur le 7000 login: admin / mdp : admin
    expose:
      - "7000"
      - "9000"
    networks:
      sonar:
      masterbet-net:
        ipv4_address: 172.20.0.30
    volumes:
      - sonarqube_conf:/opt/sonarqube/conf
      - sonarqube_data:/opt/sonarqube/data
      - sonarqube_extensions:/opt/sonarqube/extensions
    environment: 
      - sonar.jdbc.url=jdbc:postgresql://sonardb:5432/sonar
    depends_on:
      - sonardb
    profiles:
      - sonar
      - full

  sonardb:
    image: postgres
    container_name: sonardb
    restart: unless-stopped
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=sonar
    volumes:
      - sonar_postgresql:/var/lib/postgresql
      - sonar_postgresql_data:/var/lib/postgresql/data
    networks:
      sonar: {}
    profiles:
      - sonar
      - full

  runner1:
    image: gitlab/gitlab-runner:latest
    container_name: runner_docker #tag docker
    restart: unless-stopped
    depends_on:
      - mysql
      - masterbet
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.20
    volumes:
      - //var/run/docker.sock:/var/run/docker.sock
      - ./config/runner1:/etc/gitlab-runner
    profiles:
      - runner
      - sonar
      - full
      - runner_app

  runner2:
    image: gitlab/gitlab-runner:latest
    container_name: runnerQube #tag qube
    restart: unless-stopped
    depends_on:
      - mysql
      - masterbet
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.21
    volumes:
      - //var/run/docker.sock:/var/run/docker.sock
      - ./config/runner2:/etc/gitlab-runner
    profiles:
      - runner
      - sonar
      - full
      - runner_app
  
  elasticsearch:
    build:
      context: elasticsearch/
      args:
        ELK_VERSION: $ELK_VERSION
    volumes:
      - type: bind
        source: ./elasticsearch/config/elasticsearch.yml
        target: /usr/share/elasticsearch/config/elasticsearch.yml
        read_only: true
      - type: volume
        source: elasticsearch
        target: /usr/share/elasticsearch/data
    ports:
      - "9200:9200"
      - "9300:9300"
    environment:
      ES_JAVA_OPTS: "-Xmx1G -Xms1G"
      ELASTIC_PASSWORD: changeme
      # Use single node discovery in order to disable production mode and avoid bootstrap checks.
      # see: https://www.elastic.co/guide/en/elasticsearch/reference/current/bootstrap-checks.html
      discovery.type: single-node
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.50
    profiles:
      - elk
      - full

  logstash:
    build:
      context: logstash/
      args:
        ELK_VERSION: $ELK_VERSION
    volumes:
      - type: bind
        source: ./logstash/config/logstash.yml
        target: /usr/share/logstash/config/logstash.yml
        read_only: true
      - type: bind
        source: ./logstash/pipeline
        target: /usr/share/logstash/pipeline
        read_only: true
    ports:
      - "5044:5044"
      - "5000:5000/tcp"
      - "5000:5000/udp"
      - "9600:9600"
    environment:
      LS_JAVA_OPTS: "-Xmx1G -Xms1G"
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.40
    depends_on:
      - elasticsearch
    profiles:
      - elk
      - full

  kibana:
    build:
      context: kibana/
      args:
        ELK_VERSION: $ELK_VERSION
    volumes:
      - type: bind
        source: ./kibana/config/kibana.yml
        target: /usr/share/kibana/config/kibana.yml
        read_only: true
    ports:
      - "5601:5601"
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.55
    depends_on:
      - elasticsearch
    profiles:
      - elk
      - full
  
  serveur-web:
    image: nginx:1.21.3-alpine
    # redémare automatiquement sauf si coupé via cli
    restart: unless-stopped
    ports:
      - 80:80
      - 443:443
    volumes:
      # emplacement du fichier de conf de nginx
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
    networks:
      masterbet-net:
        ipv4_address: 172.20.0.60
    depends_on:
      - logstash
    profiles:
      - elk
      - full

  influxdb: #Changer de token a chaque docker compose down
    image: influxdb
    container_name: influxdb
    restart: always
    hostname: influxdb
    ports:
      - 8086:8086
    environment:
      INFLUX_DB: $INFLUX_DB  # nom de la base de données créée à l'initialisation d'InfluxDB
      INFLUXDB_USER: $INFLUXDB_USER  # nom de l'utilisateur pour gérer cette base de données
      INFLUXDB_USER_PASSWORD: $INFLUXDB_USER_PASSWORD  # mot de passe de l'utilisateur pour gérer cette base de données
    volumes:
      - ./influxdb:/var/lib/influxdb  # volume pour stocker la base de données InfluxDB
    # networks:
    #   masterbet-net:
    #     ipv4_address: 172.20.0.70
    profiles:
      - tig
      - full

  telegraf:
    image: telegraf
    depends_on:
      - influxdb  # indique que le service influxdb est nécessaire
    container_name: telegraf
    restart: always
    links:
      - influxdb:influxdb
    tty: true
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock  # nécessaire pour remonter les données du démon Docker
      - ./telegraf/telegraf.conf:/etc/telegraf/telegraf.conf  # fichier de configuration de Telegraf
    # networks:
    #   masterbet-net:
    #     ipv4_address: 172.20.0.71
    profiles:
      - tig
      - full

  grafana:
    image: grafana/grafana
    depends_on:
      - influxdb  # indique que le service influxdb est nécessaire
    container_name: grafana
    restart: always
    ports:
      - 3000:3000  # port pour accéder à l'interface web de Grafana
    links:
      - influxdb:influxdb
    environment:
      GF_INSTALL_PLUGINS: "grafana-clock-panel,\
                          grafana-influxdb-08-datasource,\
                          grafana-kairosdb-datasource,\
                          grafana-piechart-panel,\
                          grafana-simple-json-datasource,\
                          grafana-worldmap-panel"
      GF_SECURITY_ADMIN_USER: $GF_SECURITY_ADMIN_USER  # nom de l'utilisateur créé par défaut pour accéder à Grafana
      GF_SECURITY_ADMIN_PASSWORD: $GF_SECURITY_ADMIN_PASSWORD  # mot de passe de l'utilisateur créé par défaut pour accéder à Grafana
    volumes:
      - ./grafana:/var/lib/grafana
    # networks:
    #   masterbet-net:
    #     ipv4_address: 172.20.0.72
    profiles:
      - tig
      - full