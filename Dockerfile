FROM python:3.9.7

EXPOSE 5000

WORKDIR /app

COPY requirements.txt /app

RUN pip install -r requirements.txt

COPY app.py .

COPY /model/* /app/model/

COPY /controller/* /app/controller/

COPY /templates/assets/css/* /app/templates/assets/css/

COPY /templates/assets/img/* /app/templates/assets/img/

COPY /templates/* /app/templates/

RUN ls -la

CMD flask run --host=0.0.0.0