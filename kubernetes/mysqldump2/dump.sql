CREATE DATABASE  IF NOT EXISTS `masterbet` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `masterbet`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: masterbet
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (5,'Autre'),(3,'Basket'),(1,'Divers'),(2,'Foot'),(6,'Gaming'),(7,'Politique'),(4,'Rugby');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `post_id` int NOT NULL,
  `content` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`,`post_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_comment_user1_idx` (`user_id`),
  KEY `fk_comment_post1_idx` (`post_id`),
  CONSTRAINT `fk_comment_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `fk_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,2,1,'Courage Jean','2021-08-23 00:00:00',NULL),(2,2,1,'blabla test','2021-08-17 00:00:00',NULL),(3,2,1,'azertyuiop','2021-08-17 00:00:00',NULL),(5,1,2,'Test','2021-09-11 00:00:00',NULL),(6,2,21,'xxxxxxxxxxxxxxxxx','2021-08-17 00:00:00',NULL),(7,3,2,'blabla test','2021-08-17 00:00:00',NULL),(8,3,21,'qizfjpqmfjmsf','2021-08-17 00:00:00',NULL),(9,3,21,'qizfjpqmfjmsf','2021-08-17 00:00:00',NULL),(10,1,20,'blabla test','2021-08-17 00:00:00',NULL),(11,1,20,'blabla test','2021-08-17 00:00:00',NULL),(12,3,21,'zizi','2021-08-30 07:25:24',NULL),(13,2,21,'zizi','2021-08-30 15:25:58',NULL),(14,1,21,'aaa','2021-08-30 15:29:11',NULL),(15,2,20,'aaa','2021-08-30 15:29:40',NULL),(16,1,20,'aaaa','2021-08-30 15:30:48',NULL),(17,1,20,'CUL','2021-08-30 15:31:34',NULL),(18,1,21,'victorien','2021-09-01 15:17:25',NULL),(19,6,23,'Ton pari il pue le sexe','2021-09-06 18:01:06',NULL),(20,1,23,'Lol non','2021-09-06 18:01:35',NULL),(21,3,24,'J\'ai parié M5','2021-09-06 18:04:27',NULL),(22,1,24,'aaa','2021-09-06 18:04:39',NULL),(23,6,28,'caca','2021-09-08 07:19:06',NULL),(24,6,37,'Je crois en toi, test-man','2021-09-09 11:59:54',NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_has_user`
--

DROP TABLE IF EXISTS `group_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_has_user` (
  `group_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `fk_group_has_user_user1_idx` (`user_id`),
  KEY `fk_group_has_user_group_idx` (`group_id`),
  CONSTRAINT `fk_group_has_user_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `fk_group_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_has_user`
--

LOCK TABLES `group_has_user` WRITE;
/*!40000 ALTER TABLE `group_has_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `title` varchar(45) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `prediction` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_resolved` tinyint unsigned DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`,`category_id`),
  UNIQUE KEY `idpost_UNIQUE` (`id`),
  KEY `fk_post_user1_idx` (`user_id`),
  KEY `fk_post_category1_idx` (`category_id`),
  CONSTRAINT `fk_post_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `fk_post_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,1,'Ma femme','blabla','Est-ce que ma femme va me quitter dans les 3 prochains jours ?','2021-09-23 00:00:00',NULL,0),(2,2,1,'Pile ou Face','Le jeu le plus simple du monde','Pile ou Face ?','2021-10-25 00:00:00',NULL,0),(20,2,4,'C koi ?','Je c\'est po','Adakor','2021-08-25 15:44:57',NULL,0),(21,1,4,'Taille du zgeg','blabliblou','Reussi ou non ?','2021-08-26 13:28:06',NULL,0),(22,1,4,'Taille du zgeg','lifdhqlefhqlfhsf','victoire ?','2021-09-06 13:26:14',NULL,0),(23,6,7,'testou','aaaaaa','???','2021-09-06 17:27:29',NULL,1),(24,6,6,'M5 vs TeamLiquid','C\'est la finale des worlds de lol','Qui va gagner ?','2021-09-06 18:03:24',NULL,1),(25,2,7,'zzzz','xxxxxx','smkwsmfj ?','2021-09-07 12:48:23',NULL,0),(26,3,7,'qqddq','bbbbb','qdq ?','2021-09-07 12:49:03',NULL,0),(27,6,7,'Gout du chien','C koman ?','Alor di moa vilaine','2021-09-07 13:39:16',NULL,1),(28,7,7,'Le pari du 1','Je suis le test user 1','Est ce que je suis le 1 ?','2021-09-07 18:30:48',NULL,1),(29,7,7,'lolilol','ptdr','rigolol ou pa ?','2021-09-09 09:59:49',NULL,1),(30,7,7,'dqdqz','qzdzdqd','qdqzdq','2021-09-09 10:01:47',NULL,1),(31,7,7,'qdqd','qddqdq','qzddzqd','2021-09-09 10:07:19',NULL,1),(32,7,7,'qzdqzd','aaaaaaaaaa','bbbbbbb','2021-09-09 10:14:51',NULL,1),(33,7,7,'z','aaaaa','aaaa','2021-09-09 10:16:40',NULL,1),(34,7,7,'aaaaa','aaaaaaa','a','2021-09-09 10:17:51',NULL,1),(35,6,7,'Pompes','aaaaaa','aaa','2021-09-09 11:35:40',NULL,1),(36,7,7,'bb','bbb','bbb','2021-09-09 11:40:28',NULL,1),(37,7,7,'La feature','Je sais pas si ça va fonctionner ou pas...','Est-ce que ça va fonctionner ?','2021-09-09 11:46:39',NULL,1),(38,7,7,'mouahaha','aaaa','bbb','2021-09-10 14:05:31',NULL,1),(39,6,7,'test','pppp','pppp','2021-09-10 16:50:39',NULL,1),(40,6,7,'testouillance du dashboard','J\'essaye de créer un dashboard ou l\'utilisateur pourra accéder a certaines informations. Par exemple els pari qu\'il a crée et qu\'il n\'a pas encore résolu.','Samarch ?','2021-09-19 11:51:36',NULL,1),(41,6,7,'testouillance','blablabli de la testouille','agagou ?','2021-09-21 19:14:26',NULL,1);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prediction_option`
--

DROP TABLE IF EXISTS `prediction_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prediction_option` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `name` varchar(99) NOT NULL,
  `masters` int DEFAULT NULL,
  `votes_nbr` int DEFAULT NULL,
  `is_winner` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`post_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_prediction_option_post1_idx` (`post_id`),
  CONSTRAINT `fk_prediction_option_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prediction_option`
--

LOCK TABLES `prediction_option` WRITE;
/*!40000 ALTER TABLE `prediction_option` DISABLE KEYS */;
INSERT INTO `prediction_option` VALUES (1,1,'Oui / Non',200,10,0),(2,20,'On sans foot ?',6000,200,0),(3,20,'C bi1',6412,10,0),(4,21,'meh',9966,12,0),(5,21,'zgeg',790,6541,0),(6,22,'oui',407,102,1),(7,22,'non',100,100,0),(8,23,'bla',600,1,1),(9,23,'blabla',100,0,1),(10,24,'M5',3,2,1),(11,24,'Team Liquid',1,0,1),(12,24,'Le match va être cancel',1,0,0),(13,25,'Blah',0,0,0),(14,25,'blou',0,0,0),(15,26,'aa',228,3,0),(16,26,'bb',200,1,0),(17,27,'Sucré',6,6,0),(18,27,'Salé',522,3,1),(19,27,'Raciste',10,1,0),(20,28,'oui',6012,6,1),(21,28,'non',4600,2,1),(22,34,'aaa',100,1,0),(23,34,'aaa',0,0,0),(24,23,'a',0,0,0),(25,23,'a',0,0,0),(26,36,'b',100,1,1),(27,36,'b',0,0,0),(28,37,'Oui',1070,2,1),(29,37,'Non',0,0,0),(30,38,'oui',100,1,1),(31,38,'non',0,0,0),(32,39,'oui',1000,2,1),(33,39,'non',400,1,0),(34,40,'vi',10,1,1),(35,40,'na',0,0,0),(36,41,'bleh',100,1,1),(37,41,'miam',0,0,0);
/*!40000 ALTER TABLE `prediction_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `wallet` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Jean','Kul','28089','kuljean@wanadoo.net','$2b$12$rrr1oVTiCpGoWHQ4He5dHe67GG.iAN.f7ZWEvj.nh.jjO9ii4uGdu','2021-08-23 00:00:00',NULL,1),(2,'Chad','Chadman','9579','chad_the_chad@tesla.god','2sP5HD@]##``àkd??$$ç:;:','2021-08-23 00:00:00',NULL,1),(3,'Didier','Vergemol','2095','d.vergemol@yahoo.com','password','2021-08-23 00:00:00',NULL,1),(4,'ak','ka','1000','kaka@mail.com','$2b$12$3taZqe8hcq5mvOLvNlBoOOLTF80N9uZbi1iMiz9QJepKy1GagmXk2','2021-09-05 15:47:52',NULL,1),(5,'lary','lachance','1000','test@mail.com','$2b$12$887OeJvncol4SfRFhnvcS.4iuu97qm45XZ70Z1P/620KiXWA3Imla','2021-09-05 15:49:23',NULL,1),(6,'diwan','lefebvre','142','diwan@masterbet.com','$2b$12$Xj4MM7NAdL4GMGfDbTsMS.W0DdnZOx4ZArVma6SaDc4udRe94wcWK','2021-09-06 17:26:34',NULL,1),(7,'test','test','1256','1@1.1','$2b$12$eAb/xnq9I1jENB2etCfOHOa.yZfmlikcIBeQwpn7P6EAioPzSagEu','2021-09-07 16:35:03',NULL,1),(8,'test2','test2','3846','2@2.2','$2b$12$alXIQRU27iiSi53WWL/V5eDRfCy/ilREf5AmS57MAP71CBkjzcFhi','2021-09-07 16:36:16',NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_played`
--

DROP TABLE IF EXISTS `user_has_played`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_has_played` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `prediction_option_id` int NOT NULL,
  `masters_played` int unsigned NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`prediction_option_id`),
  KEY `fk_user_has_played_user1_idx` (`user_id`),
  KEY `fk_user_has_played_prediction_option1_idx` (`prediction_option_id`),
  CONSTRAINT `fk_user_has_played_prediction_option1` FOREIGN KEY (`prediction_option_id`) REFERENCES `prediction_option` (`id`),
  CONSTRAINT `fk_user_has_played_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_played`
--

LOCK TABLES `user_has_played` WRITE;
/*!40000 ALTER TABLE `user_has_played` DISABLE KEYS */;
INSERT INTO `user_has_played` VALUES (1,3,18,1),(2,1,18,500),(3,6,18,12),(4,7,19,10),(5,8,18,10),(6,6,20,10),(7,8,21,100),(8,1,20,5000),(9,6,20,2),(10,1,21,4500),(11,8,20,800),(12,7,20,100),(13,7,20,100),(14,1,22,100),(15,7,26,100),(16,1,28,1000),(17,6,28,70),(18,7,30,100),(19,6,10,1),(20,7,32,500),(21,8,33,400),(22,1,32,500),(23,6,34,10),(24,6,15,3),(25,6,36,100),(26,6,6,1);
/*!40000 ALTER TABLE `user_has_played` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-23 16:02:03
