"""
Controller des catégories
Prend les informations de l'app.py et dirige vers le model
"""

class CategoryController():
    """
    La classe contenant les méthodes relatives au controller pour les catégories
    """
    def show_categories(self, model):
        """
        Fonction de récupérer les informations relatives aux catégories
        """
        return model.get_categories()
