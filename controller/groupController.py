"""
Controller des groupes
Prend les informations de l'app.py et dirige vers le model
"""
class GroupController():
    """
    La classe contenant les méthodes relatives au controller pour les groupes
    """
    def create_group(self, model, group_name):
        """
        Méthode permettant de créer un groupe
        """
        return model.create_group(group_name)

    def get_group_id(self, model, group_name):
        """
        Méthode permettant de récupérer l'id d'un groupe
        """
        return model.get_group_id(group_name)

    def get_ids_from_email(self, model, data):
        """
        Méthode permettant de récupérer les ID d'utilisateurs depuis leurs adresse mail
        """
        mail_list= []
        for email in data:
            if email.startswith('member-'):
                var = data.get(email)
                mail_list.append(var)
        id_list = []
        for email in mail_list:
            new_id = model.get_id_from_email(email)
            id_list.append(new_id)
        return id_list

    def add_group_creator(self, model, creator_id, group_id) -> None:
        """
        Méthode permettant d'ajouter directement le créateur du
        groupe au groupe qu'il vient de créer
        """
        return model.add_group_members(group_id, creator_id)

    def add_group_members(self, model, group_id : int, list_id: list) -> None:
        """
        Méthode permettant d'aujouter au groupe les
        utilisateur ayant été précisés dans le template lors de la création du groupe
        """
        for user_id in list_id:
            model.add_group_members(group_id, user_id)

    def show_user_groups(self, model, user_id):
        """
        Méthode permettant de récupérer les groupes désquels un fait partie
        """
        return model.show_user_groups(user_id)

    def quit_group(self, model, group_id, user_id):
        """
        Méthode permettant à un utilisateur de quitter le groupe sélectioné
        """
        return model.quit_group(group_id, user_id)