"""
Controlleur pour la gestion des utilisateurs
Prend les informations de l'app.py et dirige vers le model
"""
from flask import redirect, url_for
import bcrypt

class UserController():
    """
    La classe contenant les méthodes relatives au controller pour les utilisateurs
    """
    def user_get_password(self, model, data):
        """Méthode permettant de récupérer le hash du mot de passe d'un utilisateur"""
        dataEmail = data.get('email')
        password = model.get_password(dataEmail)
        return password


    def user_login(self, model, data):
        """Méthode permettant à un utilisateur de se connecter
        avec la vérification de ses credentials"""
        global_data = model.get_user_info(data.get('email'))
        if global_data:
            db_password = self.user_get_password(model, data)
            check_pass = bcrypt.checkpw(data.get('password').encode('utf-8'),
                db_password.encode('utf-8'))
            if check_pass:
                return global_data
            return False
        return False

    def get_user_wallet(self, model, user_id):
        """Méthode permettant de récupérer les masters d'un utilisateur"""
        updated_wallet = model.get_user_wallet(user_id)
        return updated_wallet

    def playing_conditions(self, data, model, user_id):
        """Méthode vérifiant que l'utilisateur peut jouer sur un pari
        avec son solde de masters"""
        played_masters = int(data.get('masters'))
        current_player_masters = int(model.get_user_masters(user_id))

        return played_masters > 0 and played_masters <= current_player_masters

    def play(self, data, model, user_id):
        """Méthode permettant à l'utilisateur de jouer
        Ajouter un vote + retirer les master engagés par l'utilisateur
        et les ajouter à l'option + indiquer que l'utilisateur a bien parié cette option"""
        played_masters = int(data.get('masters'))
        option_id = int(data.get('option_id'))
        post_id = int(data.get('post_id'))
        model.increment_vote(option_id)
        model.substract_players_masters(played_masters, user_id)
        model.add_proposition_masters(played_masters, option_id)
        model.user_has_played(user_id, option_id, played_masters)
        return redirect(url_for('show_post', post_id=post_id))

    def user_register(self, model, data):
        """Méthode permettant à l'utilisateur de s'inscrire sur le site"""
        hashed = bcrypt.hashpw(data.get('password').encode('utf-8'), bcrypt.gensalt())
        model.user_register(data, hashed)
        return redirect(url_for('home'))

    def get_played_options(self, model, user_id):
        """Méthode permettant de récupérer toutes les options
        sur lesquelles l'utilisateur à joué"""
        played = model.get_played_options(user_id)
        return played

    def get_total_masters_played_by_user(self, model, user_id):
        """Méthode récupérant la somme de tous les masters mis en jeu
        par l'utilisateur depuis toujours"""
        return model.get_total_masters_played_by_user(user_id)

    def get_user_groups(self, model, user_id):
        """Méthode récupérant les groupes auxquels appartient l'utilisateur"""
        return model.get_user_groups(user_id)
