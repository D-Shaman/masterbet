"""
Controller des commentaires
Prend les informations de l'app.py et dirige vers le model
"""

class CommentController():
    """
    La classe contenant les méthodes relatives au controller pour les commentaires
    """
    def create_comment(self, model, data, user_id):
        """
        Méthode permetant de créer des commentaires
        """
        return model.create_comment(data, user_id)

    def delete_comment(self, model, comment_id):
        """
        Méthode permettant de supprimer des commentaires
        """
        return model.delete_comment(comment_id)
