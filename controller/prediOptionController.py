"""
Controller des prediction options
Prend les informations de l'app.py et dirige vers le model
"""
import math

class PrediOptionController():
    """
    La classe contenant les méthodes relatives au controller pour les prédiction options
    """
    def get_post_id(self, model, data, user_id):
        """
        Méthode permettant de retourner l'ID d'un pst
        """
        return model.get_post_id(data, user_id)

    def add_options(self, model, data, post_id):
        """
        Méthode permettant d'ajoputer une option a un pari
        """
        return model.add_options(data, post_id)

    def increment_vote(self, model, option_id):
        """
        Méthode permettant d'augmenter d'un vote, le nombre de vote
        """
        return model.increment_vote(option_id)

    def make_winner(self, model, option_id):
        """
        Méthode permettant de déclarer le gagnant d'un pari
        """
        return model.make_winner(option_id)

    def get_winners_id_and_masters_played(self, model, option_id):
        """
        Méthode permettant de récupérer l'ID du gagnant et la somme de masters qu'il a joué
        """
        return model.get_winners_id_and_masters_played(option_id)

    def get_total_masters(self, model, post_id):
        """
        Méthode permettant de récupérer la somme totale de masters concernant un pari
        """
        liste = []
        masters = model.get_total_masters(post_id)
        for master in masters:
            liste.append(master[0])
        return sum(liste)

    def determine_and_attribute_winnings(self, model, id_and_masters_played_winners, total_masters):
        """
        Méthode permettant de déterminer les gagnants dans une list, et de distribuer les gains
        """
        liste = []
        for item in id_and_masters_played_winners:
            liste.append(item[1])
            sum_winner = (sum(liste))

        sum_looser = total_masters - sum_winner

        for item in id_and_masters_played_winners:
            prorata_winner = ((item[1]*100)/sum_winner)
            gain_without_initial=((prorata_winner * sum_looser) / 100)
            gain_total_u = gain_without_initial + item[1]
            gain_total_t = math.trunc(gain_total_u)
            user_id = item[0]
            model.update_winner_wallet(gain_total_t, user_id)
        return 1

    def get_options_id(self, model, post_id):
        """
        Méthode permettant de récupérer les ID de toutes les options relatives a un pari
        """
        return model.get_options_id(post_id)
