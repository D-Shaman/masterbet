"""
Controller des POSTS
Prend les informations de l'app.py et dirige vers le model
"""
from flask import render_template

class PostController():
    """
    La classe contenant les méthodes relatives au controller pour les commentaires
    """
    def fetch_all_posts(self, model, categories):
        """Methode permettant de récupérer tous les posts sans distinction"""
        return render_template('home.html', paris=model.fetch_all_posts(), categories=categories)

    def fetch_all_posts_resolved(self, model, categories):
        """Methode permettant de récupérer tous les posts résolus"""
        return render_template('home2.html', paris=model.fetch_all_resolved(),
        categories=categories)

    def fetch_posts_per_categories(self, post_model, category_model, id_category, categories):
        """Méthode permettant de récupérer les posts selon leur catégorie"""
        category_name = category_model.get_category_name(id_category)
        return render_template('home2.html',
            paris=post_model.fetch_posts_per_categories(id_category),
            category_name=category_name, categories=categories)

    def fetch_posts_per_group(self, post_model, group_id):
        """Méthode permettant de récupérer les posts selon leur groupe"""
        group_name = post_model.get_group_name(group_id)
        return render_template('posts_per_group.html',
            paris=post_model.fetch_posts_per_group(group_id),
            group_name=group_name)

    def show_post(self, model, post_id, check):
        """Méthode permettant l'affichage d'un post"""
        options = model.show_post_options(post_id)
        votes_nbr = model.show_votes_nbr(post_id)
        masters_sum_of_all_options = self.calcul_somme_masters(options)
        sum_of_all_votes = self.calcul_sommes_votes(votes_nbr)
        group_name = model.get_post_group_name(post_id)
        return render_template('bet.html',\
            pari=model.show_post(post_id), comments=model.show_post_comments(post_id),
            options=options, masters_sum_of_all_options=masters_sum_of_all_options,
            sum_of_all_votes=sum_of_all_votes, check=check, group_name=group_name if group_name else '')

    def calcul_somme_masters(self, options):
        """Méthode permetaant le calcul de la somme
        de tous les masters mis en jeu sur un post"""
        liste = []
        for option in options:
            liste.append(option[2])
        return sum(liste)

    def calcul_sommes_votes(self, votes):
        """Méthode faisant la somme des votatns sur un post"""
        liste = []
        for vote in votes:
            liste.append(vote[0])
        return sum(liste)

    def create_post(self, model, data, post_id):
        """Méthode permettant la création et l'écriture en DB d'un post"""
        return model.create_post(data, post_id)

    def show_categories(self, model, data):
        """Méthode permettant l'affichage des catégories"""
        return model.show_categories(data)

    def update_post_state(self, model, post_id):
        """Méthode permettant de résoudre un post"""
        return model.update_post_state(post_id)

    def get_dashboard(self, model, user_id, name, wallet, total_masters, groups):
        """Méthode permettant l'afichage du dashboard d'un utilisateur"""
        posts = model.get_posts_from_user(user_id)
        resolved_posts = model.get_resolved_posts_from_user(user_id)
        return render_template('dashboard.html',
            posts = posts, name = name, wallet = wallet,
            total_masters = total_masters, resolved = resolved_posts, groups = groups)

    def search_posts(self, model, data):
        """Méthode permettant de chercher des posts par leur titre
        grâce à la barre de erecherche"""
        return model.search_posts(data)
