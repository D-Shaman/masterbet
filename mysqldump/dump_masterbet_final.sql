CREATE DATABASE  IF NOT EXISTS `masterbet` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `masterbet`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: masterbet
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (3,'Basketball'),(1,'Divers'),(2,'Football'),(5,'Gaming'),(6,'Politique'),(4,'Rugby'),(7,'Tech');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `post_id` int NOT NULL,
  `content` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`,`post_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_comment_user1_idx` (`user_id`),
  KEY `fk_comment_post1_idx` (`post_id`),
  CONSTRAINT `fk_comment_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `fk_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (29,10,52,'Je pense personnellement qu\'ils peuvent faire un peu plus que l\'année dernière, mais pas trop non plus, faut pas déconner ?','2021-10-29 09:41:28',NULL),(30,11,52,'Coucou Monsieur ! J\'ai parié 600 masters sur plus de 7 millions d\'euros. Grâce à Bob Lennon bien sûr ?','2021-10-29 09:44:03',NULL),(31,11,53,'Zébarti les amis ! ?','2021-10-29 09:52:19',NULL),(32,10,53,'Je dis plus que toi ! ;)','2021-10-29 09:54:14',NULL),(33,10,54,'Je me retrouve dans le sauce en terme de masters avec ce vote hihihi ?','2021-10-29 09:58:08',NULL),(34,10,55,'Diantre ! Mes masters sont à zéro. C\'est la bulle pour moi...\r\nTant pis, je voterai quand je me serai refait une santé.\r\n#Z0ZZ','2021-10-29 10:09:21',NULL),(35,14,56,'je vais gagne mon paris !!!','2021-10-29 12:01:41',NULL),(36,13,56,'Allez le Real !','2021-10-29 12:03:10',NULL),(37,16,62,'Honnêtement je sais pas.','2021-10-29 12:50:35',NULL),(38,16,54,'J\'aime pas le basket','2021-10-29 12:56:54',NULL),(39,16,55,'Pas Zemmour par pitié !','2021-10-29 12:57:08',NULL),(40,17,56,'Le Barca les gars !','2021-10-29 12:58:46',NULL),(41,17,54,'T\'as tort, Mel ! Avec Diwan on se couche hyper tard le soir pour voir nos joueurs préférés suer sur les planches !','2021-10-29 12:59:29',NULL),(42,12,54,'T\'es sérieux Lucas ?! Tu vois Jason Tatum suer en faisant une tirade de Shakespeare ? -_-\" C\'est sur le PARQUET !','2021-10-29 13:00:46',NULL),(43,12,61,'Il me faudrait d\'abord en voir une démontration en temps réel...','2021-10-29 13:01:41',NULL),(44,13,61,'oui mais ca ne marche pas','2021-10-29 13:03:57',NULL),(45,13,62,'L\'Irlande, pour la Guinness bien sûr ! ?','2021-10-29 13:05:05',NULL),(46,13,55,'Vas-y JONLUK !','2021-10-29 13:05:50',NULL),(47,13,57,'Tu n\'y arriveras jamais, Popol.','2021-10-29 13:06:06',NULL),(48,12,57,'T\'es vache avec Pwol, Clem. Je suis certain qu\'il y arrivera en moins d\'une journée ! Let\'s go Pwol ?','2021-10-29 13:07:25',NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (44,'? Les testeurs en folie ! ?'),(45,'DevOps'),(46,'Poupipou');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_has_user`
--

DROP TABLE IF EXISTS `group_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_has_user` (
  `group_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `fk_group_has_user_group_idx` (`group_id`),
  KEY `fk_group_has_user_user1_idx` (`user_id`),
  CONSTRAINT `fk_group_has_user_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `fk_group_has_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_has_user`
--

LOCK TABLES `group_has_user` WRITE;
/*!40000 ALTER TABLE `group_has_user` DISABLE KEYS */;
INSERT INTO `group_has_user` VALUES (44,10),(44,11),(45,12),(45,13);
/*!40000 ALTER TABLE `group_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `prediction` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_resolved` tinyint unsigned DEFAULT '0',
  `group_id` int NOT NULL DEFAULT '0',
  `prize` varchar(99) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`,`category_id`,`group_id`),
  UNIQUE KEY `idpost_UNIQUE` (`id`),
  KEY `fk_post_user1_idx` (`user_id`),
  KEY `fk_post_category1_idx` (`category_id`),
  KEY `fk_post_group1_idx` (`group_id`),
  CONSTRAINT `fk_post_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `fk_post_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (52,10,5,'Combien va rapporter le ZEvent 2021 ?','Salut à tous !\r\nJe pense que le situation se prête bien aux paris, sur ce magnifique site (Créé par les dieux du dev Diwan et Clément ngl)\r\nCombien pensez-vous que nos streamers hexagonaux (ou maltais) préférés vont réussir à lever pendant le ZEvent ?\r\nA vos masters !',NULL,'2021-10-29 09:38:32',NULL,1,0,NULL),(53,11,5,'Zevent: Combien de temps avant le premier million de dons ?','Je pense que tout est dit !\r\nNe soyez pas trop nombreux à gagner SVP MDR',NULL,'2021-10-29 09:51:49',NULL,0,44,'Je fais un don au ZEvent (25€ par gagnant)'),(54,10,3,'Taux de victoire des Celtics en SR cette année ?','Avis aux amateurs du ballon orange !\r\nL\'année dernière, l\'équipe a eu un taux de 50% de victoires (41 matchs gagnés)\r\nPensez-vous que cette année sera encore meilleure ?\r\nLezgongue les gars !',NULL,'2021-10-29 09:57:26',NULL,0,0,NULL),(55,13,6,'Qui va gagner les élections 2022 ?','La question a le mérite de se poser en cette pré-campagne !\r\nAttention aux débats dans les commentaires svp. Soyez respectueux entre vous ;)',NULL,'2021-10-29 10:05:57',NULL,0,0,NULL),(56,12,2,'Résultat du clasico','Ce soir, FC Barcelone VS Real Madrid\r\nQui des Madrilènes ou des Barcelonais vont gagner ?',NULL,'2021-10-29 11:59:22',NULL,0,0,NULL),(57,15,1,'Combien de temps me faut-il pour apprendre à faire un backflip ?','Salut ! Maman m\'a donné le défi de savoir faire un backflip (arrière) et j\'ai accepté.\r Je vais apprendre et m\'entrainer tous les jours jusqu\'à atteindre mon objectif ! ?‍♀️ Combien de temps pensez-vous que cela me nécessitera ?',NULL,'2021-10-29 12:23:39',NULL,0,0,NULL),(61,14,7,'Est-ce qu\'une version du metaverse va sortir avant la fin de l\'année ?','Le Metaverse est-il quelque chose qu\'il faut attendre ou craindre ? Là n\'est pas la question. Je veux savoir ce que vous pensez du fait qu\'une version puisse sortir avant la fin de l\'année 2021',NULL,'2021-10-29 12:41:31',NULL,0,0,NULL),(62,16,4,'Quelle équipe va gagner le tournoi des 6 nations 2022 ?','Salut à tous ! Le Rugby, c\'est mon dada, alors je voulais faire un petit pari sur l\'équipe qui gagnera le prochain Tournoi des six Nations.\r\nC\'est parti !',NULL,'2021-10-29 12:50:22',NULL,0,0,NULL),(63,12,5,'Final des ESL','Ce soir c\'est la finale des ESL, qui c\'est qui gagne a votre avis ?! ?',NULL,'2021-11-05 15:37:58',NULL,0,0,NULL),(64,12,2,'Lille vs Lens','Ce soir, c\'est le match entre Lille et Lens, moi perso, je viens de Bretagne, donc au final... J\'m\'en tape ! Mais je sais que vous aimez le foot. ✌?\r\n\r\nQui va gagner ce soir ?',NULL,'2021-11-05 15:45:04',NULL,0,0,NULL),(65,12,1,'La pièce de théâtre \"Othello\" par \'Shakespear\' s\'ouvre ce soir.','Combien de spectateur dans la salle ? ?',NULL,'2021-11-08 12:42:05',NULL,0,0,NULL);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prediction_option`
--

DROP TABLE IF EXISTS `prediction_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prediction_option` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `name` varchar(99) NOT NULL,
  `masters` int DEFAULT NULL,
  `votes_nbr` int DEFAULT NULL,
  `is_winner` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`post_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_prediction_option_post1_idx` (`post_id`),
  CONSTRAINT `fk_prediction_option_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prediction_option`
--

LOCK TABLES `prediction_option` WRITE;
/*!40000 ALTER TABLE `prediction_option` DISABLE KEYS */;
INSERT INTO `prediction_option` VALUES (63,52,'moins de 3 millions',0,0,0),(64,52,'entre 3 et 5 millions',0,0,0),(65,52,'entre 5 et 7 millions',500,1,0),(66,52,'plus de 7 millions',600,1,1),(67,53,'moins d\'1h',0,0,0),(68,53,'entre 1h et 2h',0,0,0),(69,53,'entre 2h et 5h',0,0,0),(70,53,'entre 5 et 10h',200,1,0),(71,53,'entre 10h et un jour',300,1,0),(72,53,'plus d\'un jour',0,0,0),(73,54,'-50%',0,0,0),(74,54,'+50%',200,1,0),(75,54,'0%',0,0,0),(76,54,'100%',0,0,0),(77,55,'Emmanuel Macron',0,0,0),(78,55,'Marine Le Pen',300,1,0),(79,55,'Anne Hidalgo',0,0,0),(80,55,'Jean-Luc Mélenchon',500,1,0),(81,55,'Eric Zemmour',42,1,0),(82,55,'Yannick Jadot',200,1,0),(83,55,'Fabien Roussel',0,0,0),(84,55,'Arnaud Montebourg',0,0,0),(85,55,'Xavier Bertrand',0,0,0),(86,55,'Valérie Pécresse',0,0,0),(87,56,'Les Espagnols',275,2,0),(88,56,'Les Catalans',281,2,0),(89,57,'Moins d\'un jour',0,0,0),(90,57,'Un jour',0,0,0),(91,57,'Deux jours',0,0,0),(92,57,'Entre trois jours et une semaine',0,0,0),(93,57,'Plus d\'une semaine',0,0,0),(97,61,'oui',0,0,0),(98,61,'non',125,1,0),(99,62,'France',0,0,0),(100,62,'Angleterre',0,0,0),(101,62,'Irlande',250,1,0),(102,62,'Nouvelle-Zélande',0,0,0),(103,62,'Australie',0,0,0),(104,62,'Ecosse',0,0,0),(105,63,'SKT T1',0,0,0),(106,63,'Team Liquid',0,0,0),(107,63,'Veritas',0,0,0),(108,63,'Navi',0,0,0),(109,63,'Fnatic',0,0,0),(110,64,'Lille, facilement',0,0,0),(111,64,'Lens, c\'est sur',0,0,0),(112,65,'Quasiment personne ',0,0,0),(113,65,'La salle sera pleine !',0,0,0);
/*!40000 ALTER TABLE `prediction_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `wallet` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (10,'Monsieur','Test','0','test@mb.com','$2b$12$oPLzow4.iMEQ5BGojyMm2eqYBd57.AgT5nhWZqG1RRc7y2fgp6kMK','2021-10-29 09:32:39',NULL,1),(11,'Madame','Teste','1000','teste@mg.com','$2b$12$1P0JFph.s3TSX7vrRHOV8.wCZovfm20EBApVqNBP5OWkbDKGWuUJy','2021-10-29 09:42:45',NULL,1),(12,'Diwan','Lefebvre','650','diwan@mb.com','$2b$12$0WN4HaUjWzhtwMXhBCuFlu4J0UFTDZkCNWMIxGVQp.G4JmrfdE3cy','2021-10-29 09:59:36',NULL,1),(13,'Clément','Markey','1000','clement@mb.com','$2b$12$9CdHF0L/zldkUO2SFkAcTe31QmWxJBkgdW8XkV0PzBtqM0HCY7j5i','2021-10-29 09:59:55',NULL,1),(14,'Nominique','Dossant','800','dominique@vim.vi','$2b$12$McYjamqig04ZLW9l0jKwzu/A6jbirzzrr.VnUKQ8TKUTmh8hHX3OC','2021-10-29 10:00:53',NULL,1),(15,'Paul','Houriez','1000','popol@gmail.com','$2b$12$DP4ptM.tdoqQ.Bt2su44Ae2uNP2AH2tvq5CqwH8w3xVE6upQF.04u','2021-10-29 12:15:15',NULL,1),(16,'Mélisande','Baratte','1000','melisande@gmail.com','$2b$12$Ve1Me2x3m.sd9775E2CnBuKXFLiZSHtj/.0CEZRv4QafMamOxi8oy','2021-10-29 12:44:12',NULL,1),(17,'Lucas','Dubart','877','lucas@gmail.com','$2b$12$/5meTsIgQb334bY4x1OfBOX3tXaVVs3oT97s4VHssp223LdmTTHru','2021-10-29 12:57:38',NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_played`
--

DROP TABLE IF EXISTS `user_has_played`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_has_played` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `prediction_option_id` int NOT NULL,
  `masters_played` int unsigned NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`prediction_option_id`),
  KEY `fk_user_has_played_user1_idx` (`user_id`),
  KEY `fk_user_has_played_prediction_option1_idx` (`prediction_option_id`),
  CONSTRAINT `fk_user_has_played_prediction_option1` FOREIGN KEY (`prediction_option_id`) REFERENCES `prediction_option` (`id`),
  CONSTRAINT `fk_user_has_played_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_played`
--

LOCK TABLES `user_has_played` WRITE;
/*!40000 ALTER TABLE `user_has_played` DISABLE KEYS */;
INSERT INTO `user_has_played` VALUES (31,10,65,500),(32,11,66,600),(33,11,70,200),(34,10,71,300),(35,10,74,200),(36,13,80,500),(37,14,81,42),(38,12,82,200),(39,11,78,300),(40,12,87,150),(41,14,88,158),(42,13,87,125),(43,17,88,123),(44,13,98,125),(45,13,101,250);
/*!40000 ALTER TABLE `user_has_played` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-11 11:17:31
