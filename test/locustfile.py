
from locust import HttpUser, task, between
try:
    import json
except ImportError:
    import simplejson as json

class WebsiteTestUser(HttpUser):
    wait_time = between(0.5, 3.0)

    def on_start(self):
        print("Début de la phase de load test")

    def on_stop(self):
        print("Fin de la phase de load test")

    @task(1)
    def test_home(self):
        self.client.get("http://localhost:5000")
    
    @task(2)
    def test_login(self):
        print("Connexion")
        data = json.dumps({"email" : "1@1.1", "password" : "1"})
        self.client.post("http://localhost:5000/user_sign_in", data)
        print("Déconnexion")
        self.client.get("http://localhost:5000/logout")