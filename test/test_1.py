from datetime import datetime

# Command is "py -m pytest -rP > ./test/test_logs.log"

def test_index(client):
    res = client.get('/')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == normal_behaviour

def test_login(client):
    res = client.get('/login')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /login")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_register(client):
    res = client.get('/register_page')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /register_page")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_display_bet(client):
    res = client.get('/post/42')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /post/1")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_post_form(client):
    res = client.get('/post_form')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /post_form")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_create_post(client):
    res = client.get('/create_post')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /create_post")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_display_resolved_posts(client):
    res = client.get('/resolved_posts')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /resolved_posts")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_resolution_pari(client):
    res = client.get('/set_winner/1/2')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /set_winner/1/2, test de la fonction de résolution")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200

def test_dashboard(client):
    res = client.get('/dashboard')
    normal_behaviour = 200
    print(datetime.utcnow())
    print("Test de la route /dashboard")
    print("Reponse attendue : {}".format(normal_behaviour))
    print("Reponse obtenue : {}".format(res.status_code))
    if normal_behaviour == res.status_code:
        print("Behaviour is normal")
    assert res.status_code == 200