import json

def test_register(client):
    normal_behavior = 200
    data = {
        "name": "Jacques",
        "last_name": "Louis",
        "email": "jacques.l@gmail.com",
        "password": "1044"
    }
    res = client.post(
        '/user_register',
        data=json.dumps(data))
    assert res.status_code == normal_behavior or res.status_code == 302