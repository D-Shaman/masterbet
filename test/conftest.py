try:
    import pytest
    from app import app as flask_app
except Exception as e:
    print("blabla sa marsh pas : {}".format(e))

@pytest.fixture
def app():
    yield flask_app

@pytest.fixture
def client(app):
    return app.test_client()