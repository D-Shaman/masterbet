"""Modèle des groupes"""
from model.db import Connect

class GroupModel():
    """
    La classe contenant les méthodes relatives au modèle pour les commentaires
    """

    def __init__(self):
        self.conn = Connect.log()

    def create_group(self, group_name):
        """Méthode permettant de créer un groupe et de l'inscrire en DB"""
        self.conn.execute(f'''
        INSERT INTO masterbet.group(name)
        VALUES ('{group_name}')
        ''')

    def get_group_id(self, group_name):
        """Méthode permettant de récupérer l'id d'un groupe"""
        self.conn.execute(f'''
        SELECT id
        FROM masterbet.group
        WHERE name = '{group_name}'
        ''')
        row = self.conn.fetchone()
        return row[0]

    def get_id_from_email(self, email):
        """Méthode permettant la récupération de l'id
        d'un utilisateur à partir de son email"""
        self.conn.execute(f'''
        SELECT id
        FROM masterbet.user
        WHERE email = '{email}'
        ''')
        row = self.conn.fetchone()
        return row[0]

    def add_group_members(self, group_id : int, creator_id : int) :
        """Méthode permettant l'ajout d'utilisateurs à un groupe"""
        self.conn.execute(f'''
        INSERT INTO group_has_user(group_id, user_id)
        VALUES ({group_id}, {creator_id})
        ''')

    def show_user_groups(self, user_id : int):
        """Méthode permettant la récupération des groupes d'un utilisateur"""
        self.conn.execute(f'''
        SELECT name, id
        FROM masterbet.group
        INNER JOIN group_has_user
        ON group.id = group_id
        WHERE group_has_user.user_id = {user_id}
        ''')
        rows = self.conn.fetchall()
        return rows

    def quit_group(self, group_id, user_id):
        """
        Méthode permettant la suppression d'un ligne user_has_group
        Et donc de supprimer un utilisateur d'un groupe
        """
        self.conn.execute(f"""
        DELETE FROM masterbet.group_has_user
        WHERE group_id = {group_id}
        AND user_id = {user_id}
        """)
