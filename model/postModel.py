"""
Méthodes utilisées pour les posts
"""

from datetime import datetime
from model.db import Connect

class PostModel():
    """
    Classe contenant les méthodes relatives aux posts
    """
    def __init__(self):
        self.conn = Connect.log()

    def fetch_all_posts(self):
        """
        Méthode permetant de récupérer les informations des posts en db
        """
        self.conn.execute('''
        SELECT post.id, title, post.content, prediction, post.created_at, user.name, user.last_name, category_id, category.name 
        FROM post 
        INNER JOIN user ON (user_id=user.id)
        INNER JOIN category on (category_id = category.id)
        WHERE is_resolved = 0
        AND group_id = 0
        ORDER BY post.id DESC
        ''')
        rows = self.conn.fetchall()
        return rows

    def fetch_all_resolved(self):
        """
        Méthode permetant de récupérer les informations de tous le sposts résolus
        """
        self.conn.execute('''
        SELECT post.id, title, post.content, prediction, post.created_at, user.name, user.last_name, is_resolved, category.name
        FROM post 
        INNER JOIN user ON (user_id=user.id)
        INNER JOIN category ON (category_id=category.id)
        WHERE is_resolved = 1
        AND group_id = 0
        ORDER BY post.id DESC
        ''')
        rows = self.conn.fetchall()
        return rows

    def fetch_posts_per_categories(self, id_category):
        """
        Méthode permettant de récupérer les informations sur le posts en filtrant par catégorie
        """
        self.conn.execute('''
        SELECT post.id, title, post.content, prediction, post.created_at, user.name, user.last_name
        FROM post 
        INNER JOIN user ON (user_id=user.id)
        WHERE category_id = %s
        AND group_id = 0
        ORDER BY post.id DESC
        ''', (id_category,))
        rows = self.conn.fetchall()
        return rows

    def get_post_id(self, user_id, data):
        """
        Méthode permettant de récupérer l'ID d'un post
        """
        self.conn.execute('''
        SELECT id FROM post
        WHERE user_id = %s AND content = %s
        ''',(
        user_id,
        data.get('content')
        ))
        post_id = self.conn.fetchone()
        return post_id[0]

    def create_post(self, data, post_id):
        """
        Méthode permettant de créer un post
        """
        self.conn.execute('''
        INSERT INTO post (user_id, category_id, title, content, created_at, group_id, prize) 
        VALUES (%s, %s, %s, %s, %s, %s, %s)''',(
        post_id,
        data.get('category_id'),
        data.get('title'),
        data.get('content'),
        datetime.utcnow(),
        data.get('group_id') if data.get('group_id') else 0,
        data.get('prize') if data.get('prize') else None))

    def show_post(self, post_id):
        """
        Méthode permetant de récupérer les informations d'un post spécifique
        """
        self.conn.execute(f'''
        SELECT post.id, user.name, user.last_name, post.title, post.content, post.prediction, post.created_at, user.id, is_resolved, category.name, prize
        FROM post
        INNER JOIN user on user.id = post.user_id
        INNER JOIN category on category.id = post.category_id
        WHERE post.id = {post_id}
        ''')
        rows = self.conn.fetchall()
        return rows[0]

    def show_post_comments(self, post_id):
        """
        Méthode permettant de récupérer les informations des commentaires
        """
        self.conn.execute(f'''
        SELECT user.name, user.last_name, comment.content, comment.created_at, comment.id, user.id
        from comment
        INNER JOIN user on user.id = comment.user_id
        INNER JOIN post on post.id = comment.post_id
        WHERE post.id = {post_id}
        ORDER BY comment.created_at ASC
        ''')
        rows = self.conn.fetchall()
        return rows

    def show_post_options(self, post_id):
        """
        Méthode permettant de récupérer les prédictions possibles pour un post spécifique
        """
        self.conn.execute(f'''
        SELECT prediction_option.id, prediction_option.name, prediction_option.masters, prediction_option.votes_nbr from prediction_option
        WHERE post_id = {post_id}
        ''')
        rows = self.conn.fetchall()
        return rows

    def show_votes_nbr(self, post_id):
        """
        Méthode permettant de récupérer le nombre de votes d'une option
        """
        self.conn.execute(f'''
        SELECT votes_nbr
        FROM prediction_option
        WHERE post_id = {post_id}
        ''')
        rows = self.conn.fetchall()
        return rows

    def update_post_state(self, post_id):
        """
        Méthode permettant de changer l'état d'un post vers resolved
        """
        self.conn.execute('''
        UPDATE post
        SET is_resolved = 1
        WHERE id = %s
        ''', (post_id,))

    def get_posts_from_user(self, user_id):
        """
        Méthode permettant de récupérer les posts spécifique a un utilisateur
        """
        self.conn.execute(f'''
        SELECT post.id, title, post.content, prediction, post.created_at, user.name, user.last_name, category_id, category.name, is_resolved
        FROM post
        INNER JOIN user ON (user_id=user.id) 
        INNER JOIN category ON (category_id=category.id) 
        WHERE user_id = {user_id}
        ORDER BY is_resolved ASC, post.created_at DESC
        ''')
        rows = self.conn.fetchall()
        return rows

    def get_resolved_posts_from_user(self, user_id):
        """
        Méthode permettant de récupérer les posts récolu spécifique a un utilisateur
        """
        self.conn.execute(f'''
        SELECT COUNT(*)
        FROM post
        WHERE user_id = {user_id}
        AND is_resolved = 1
        ''')
        rows = self.conn.fetchone()
        return rows[0]

    def fetch_posts_per_group(self, group_id):
        """
        Méthode permettant de récupérer les posts spécifique a un groupe
        """
        self.conn.execute(f"""
        SELECT post.id, title, post.content, post.created_at, user.name, user.last_name
        FROM post 
        INNER JOIN masterbet.group ON (group_id={group_id})
        INNER JOIN masterbet.user ON post.user_id = user.id
        WHERE group_id = masterbet.group.id
        AND is_resolved = 0
        ORDER BY post.id DESC;
        """)
        rows = self.conn.fetchall()
        return rows

    def get_group_name(self, group_id):
        """
        Méthode permettant de récupérer le nom d'un groupe par son ID
        """
        self.conn.execute(f"""
        SELECT name
        FROM masterbet.group
        WHERE id = '{group_id}'
        """)
        row = self.conn.fetchone()
        return row[0]

    def get_post_group_name(self, post_id):
        """
        Méthode permettant de récupérer les posts spécifique a un groupe
        """
        try:
            self.conn.execute(f"""
            SELECT group.name
            FROM masterbet.group
            INNER JOIN post on post.group_id = masterbet.group.id
            WHERE post.id = '{post_id}'
            """)
            row = self.conn.fetchone()
            return row[0]
        except Exception:
            return None

    def search_posts(self, data):
        """
        Méthode permettant de récupérer un ensemble de post par similarité dans le titre
        """
        print("Coucou du model")
        self.conn.execute(f"""
        SELECT post.id, title, post.content, prediction, post.created_at, user.name, user.last_name, category_id, category.name 
        FROM post
        INNER JOIN user ON (user_id=user.id)
        INNER JOIN category on (category_id = category.id)
        WHERE post.title
        LIKE '%{data}%'
        ORDER BY post.id DESC
        """)
        rows = self.conn.fetchall()
        return rows
