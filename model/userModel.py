"""Modèle des utilisateurs"""
from datetime import datetime
from model.db import Connect

class UserModel():
    """
    La classe contenant les méthodes relatives au modèle pour les utilisateurs
    """
    def __init__(self):
        self.conn = Connect.log()

    def get_password(self, data):
        """Méthode récupérant le hash du mot de passe d'un utilisateur
        d'après son email"""
        self.conn.execute("""
        SELECT password
        FROM user
        WHERE email = %s
        """, (data,))
        rows = self.conn.fetchone()
        return rows[0]

    def get_user_info(self, data):
        """Méthode récupérant toutes les informations d'un utilisateur
        nécessaires à sa connexion"""
        self.conn.execute('''
        SELECT *
        FROM user
        WHERE email = %s
        ''', (data,))
        rows = self.conn.fetchall()
        if rows != []:
            return rows
        return False

    def check_login(self, user_credentials):
        """Méthode vérifiant la si l'utilisateur est bien connecté"""
        self.conn.execute('''
        SELECT email, password
        FROM user
        WHERE email = %s
        AND password = %s
        ''',(user_credentials[0], user_credentials[1].decode('utf-8')))
        rows = self.conn.fetchone()
        if rows:
            return rows[0]
        return 0

    def get_user_masters(self, user_id):
        """Méthode récupérant le solde de masters d'un utilisateur"""
        self.conn.execute(f'''
        SELECT wallet
        FROM user
        WHERE id = {user_id}
        ''')
        rows = self.conn.fetchone()
        return rows[0]

    def substract_players_masters(self, played_masters, user_id):
        """Méthode soustrayant les masters à un utilisateur"""
        self.conn.execute('''
        UPDATE user
        SET wallet = wallet - %s
        WHERE id = %s
        ''',(played_masters, user_id))

    def add_proposition_masters(self, played_masters, option_id):
        """Méthode ajoutant des masters pariés sur une option"""
        self.conn.execute('''
        UPDATE prediction_option
        SET masters = masters + %s
        WHERE id = %s
        ''',(played_masters, option_id))

    def user_register(self, data, hashed):
        """Méthode inscrivant un utilisateur en DB"""
        self.conn.execute('''
        INSERT INTO user (name, last_name, wallet, email, password, created_at, is_active)
        VALUES (%s, %s, %s, %s, %s ,%s, %s)''', (
            data.get('name'),
            data.get('last_name'),
            ('1000'),
            data.get('email'),
            hashed,
            datetime.utcnow(),
            ('1')))

    def increment_vote(self, option_id):
        """Méthode incrémentant une option de 1"""
        self.conn.execute('''
        UPDATE prediction_option
        SET votes_nbr = votes_nbr + 1
        WHERE id = %s
        ''', (option_id,))

    def user_has_played(self, user_id, option_id, played_masters):
        """Méthode déclarant un utilisateur comme ayant joué une option"""
        self.conn.execute('''
        INSERT INTO user_has_played (user_id, prediction_option_id, masters_played)
        VALUES (%s, %s, %s)
        ''', (user_id, option_id, played_masters))

    def get_played_options(self, user_id):
        """Méthode récupérant l'id des options de prédictions jouées par un utilisateur"""
        self.conn.execute(f'''
        SELECT prediction_option_id
        FROM user_has_played
        WHERE user_id = {user_id}
        ''')
        rows = self.conn.fetchall()
        return rows

    def get_total_masters_played_by_user(self, user_id):
        """Méthode récupérant la somme totale des masters
        joués par un utilisateur"""
        self.conn.execute(f'''
        SELECT SUM(masters_played)
        FROM user_has_played
        WHERE user_id = {user_id}
        ''')
        rows = self.conn.fetchone()
        return rows[0]

    def get_user_groups(self, user_id):
        """Méthode récupérant les groupes auxquels appartient un utilisateur"""
        self.conn.execute(f"""
        SELECT masterbet.group.id, masterbet.group.name
        FROM masterbet.group
        INNER JOIN masterbet.group_has_user on masterbet.group_has_user.group_id = masterbet.group.id
        WHERE user_id = {user_id};
        """)
        rows = self.conn.fetchall()
        return rows

    def get_user_wallet(self, user_id):
        """Méthode récupérant les masters d'un utilisateur en DB
        dans le but de mettre le solde à jour"""
        self.conn.execute(f"""
        SELECT wallet
        FROM user
        WHERE id = {user_id}
        """)
        row = self.conn.fetchone()
        return row[0]
