"""
Modèle des catégories
"""

from model.db import Connect

class CategoryModel():
    """
    Classe contenant les modèles des catégories, informations venant du controller
    """
    def __init__(self):
        self.conn = Connect.log()

    def get_categories(self):
        """
        Méthodes permettant d'obtenir les informations sur les catégories
        """
        self.conn.execute('''
        SELECT *
        FROM category
        ORDER BY id ASC
        ''')
        rows = self.conn.fetchall()
        return rows

    def get_category_name(self, cat_id):
        """
        Méthode permettant de récupérer le nom des catégories
        """
        self.conn.execute(f'''
        SELECT name
        FROM category
        WHERE id = {cat_id}
        ''')
        rows = self.conn.fetchone()
        return rows[0]
