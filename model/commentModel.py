"""Modèle des commentaires"""

from datetime import datetime
from model.db import Connect

class CommentModel():
    """
    La classe contenant les méthodes relatives au modèle pour les commentaires
    """
    def __init__(self):
        self.conn = Connect.log()

    def create_comment(self, data, user_id):
        """Méthode inscrivant un commentaire en base de données"""
        self.conn.execute("""
        INSERT INTO comment (user_id, post_id, comment.content, comment.created_at)
        VALUES (%s, %s, %s, %s)
        """,(
            user_id,
            data.get('post_id'),
            data.get('content'),
            datetime.utcnow()))

    def delete_comment(self, comment_id):
        """Méthode supprimant un commentaire en base de données"""
        self.conn.execute(f"""
        DELETE FROM comment
        WHERE id = {comment_id}
        """)
