"""
Model des prédictions
"""

from model.db import Connect

class PrediOptionModel():
    """
    Modèle contenant les méthodes pour les prédictions. Communication avec le controller
    """
    def __init__(self):
        self.conn = Connect.log()

    def add_options(self, data, post_id):
        """
        Méthode permettant d'ajouter des options a un pari, deruant sa création
        """
        print(post_id)
        for i in data:
            if i.startswith('option-'):
                self.conn.execute('''
                INSERT INTO prediction_option(post_id, name, masters, votes_nbr)
                VALUES (%s, %s, %s, %s)
                ''',(
                    post_id,
                    data.get(i),
                    0,
                    0
                ))

    def make_winner(self, option_id):
        """
        Méthode permettant de déclarer une option comme étant gagnante
        """
        self.conn.execute('''
        UPDATE prediction_option
        SET is_winner = 1
        WHERE id = %s
        ''', (option_id,))

    def get_winners_id_and_masters_played(self, option_id):
        """
        Méthode permettant de récupérer l'ID d'un utilisateur gagant et les masters qu'il a engagé
        """
        self.conn.execute('''
        SELECT user_id, masters_played
        FROM user_has_played
        WHERE prediction_option_id = %s
        ''', (option_id,))
        rows = self.conn.fetchall()
        return rows

    def get_total_masters(self, post_id):
        """
        Méthode permettant de récupérer la somme totale de masters engagés dans un pari
        """
        self.conn.execute('''
        SELECT masters
        FROM prediction_option
        WHERE post_id = %s
        ''', (post_id,))
        rows = self.conn.fetchall()
        return rows

    def update_winner_wallet(self, gain_total, user_id):
        """
        Méthode permettant d'updater le portefeuille de masters d'un utilisateur
        """
        self.conn.execute('''
        UPDATE user
        SET wallet = wallet + %s
        WHERE id = %s
        ''', (gain_total, user_id))

    def get_options_id(self, post_id):
        """
        Méthode permettant de récupérer les ids des prédiction option
        """
        self.conn.execute(f'''
        SELECT id
        FROM prediction_option
        WHERE post_id = {post_id}
        ''')
        rows = self.conn.fetchall()
        return rows
