"""
Ce fichier permet la communication entre la db et l'application.
"""
from abc import ABC
import mysql.connector

class Connect(ABC):
    """
    Classe abstraite de connection pour mysql-connector-python
    """
    conn = mysql.connector.connect(
        host="localhost", #Localhost pour la version non dockerisée,
        #mysql pour la version dockerisée (docker-compose), mysql-service pour la version KUBE
        user="root",
        password="root",
        database="masterbet",
        port="3306"
    )
    conn.autocommit = True

    @staticmethod
    def log():
        """
        jsp
        """
        try:
            cursor = Connect.conn.cursor(buffered=True)
            return cursor
        except mysql.connector.Error as err :
            return print(err)
