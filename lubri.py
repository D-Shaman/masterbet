"""
Lubri est un script d'aide au lancement de l'application Masterbet dans toutes ses configurations.
"""
import sys
import os
import time
import webbrowser
from subprocess import Popen, CREATE_NEW_CONSOLE

def choice_func():
    """
    Fonction pour le premier choix de l'utilisateur
    """
    print("Bien le bonjour maîîîîître... Je suis Lubri, votre assistant personnel pour le projet masterbet...")
    time.sleep(2)
    print("Que souhaitez-vous faire ?")
    print("0 - Tuer Lubri")
    print("1 - Lancer l'application en local.")
    print("2 - Lancer l'application contenerisée, en configuration minimale.")
    print("3 - Lancer les runners uniquement.")
    print("4 - Lancer les runners et l'application.")
    print("5 - Lancer SonarQube et les runners.")
    print("6 - Lancer l'outil de log (stack ELK, proxy Nginx).")
    print("7 - Lancer l'outil de monitoring (stack TIG)")
    print("8 - Down un container.")
    print("9 - Lancer la stack Kubernetes.")
    print("10 - Down la stack Kubernetes.")
    print("11 - Executer des tests.")
    val = input("Quel est vôtre choix ? ")
    return int(val)

def action(val):
    """
    Fonction qui mène aux actions que peut effectuer Lubri, mais aussi aux routes de second choix
    """
    if val == 0:
        sys.exit()
    elif val == 1:
        try:
            if os.environ['VIRTUAL_ENV']:
                port = input('Sur quel port souhaitez vous lancer Masterbet ? ')
                print("Tout de suite, maître")
        except Exception:
            print("Veuillez activer l'environnment virtuel en lançant la commande ./venv/Scripts/activate")
            print("*Lubri retourne dans son placard*")
            sys.exit()

        url_list = ["http://localhost:{}".format(port)]
        open_browser(url_list)
        Popen("Flask run --reload -p{}".format(port), creationflags=CREATE_NEW_CONSOLE)

    elif val == 2:
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile min up -d")
        url_list = ["http://localhost:5000"]
        open_browser(url_list)

    elif val == 3:
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile runner up -d")
        url_list = ["https://gitlab.com/D-Shaman/masterbet"]
        open_browser(url_list)

    elif val == 4:
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile runner_app up -d")
        url_list = ["https://gitlab.com/D-Shaman/masterbet", "http://localhost:5000" ]
        open_browser(url_list)

    elif val == 5:
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile sonar up -d")
        url_list = ["https://gitlab.com/D-Shaman/masterbet", "http://localhost:7000"]
        open_browser(url_list)

    elif val == 6:
        print("Veuillez verifier que dans le docker-compose, la section \"ports\" de masterbet_app a été commentée")
        input("Appuyez sur une touche quand la vérification a été faite.")
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile elk up -d")
        time.sleep(7)
        url_list = ["http://localhost:80", "http://localhost:5601"]
        open_browser(url_list)

    elif val == 7:
        print("Bien maîîîître... Je vais faire venir les conteneurs...")
        os.system("docker compose --profile tig up -d")
        url_list = ["http://localhost:8086"]

    elif val == 8:
        print('Quel est la stack que vous voulez down ?')
        print("1 - Config minimale")
        print("2 - Les runners uniquement")
        print("3 - Les runners et l'app")
        print("4 - La stack SonarQube.")
        print("5 - La stack ELK.")
        print("6 - La stack TIG.")
        input_stack_down = input("Quel est vôtre choix ? ")
        container_down(int(input_stack_down))

    elif val == 9:
        os.system("cd ./kubernetes && kubectl apply -f .")
        os.system("cd ./kubernetes && kubectl apply -f .")

    elif val == 10:
        os.system("kubectl delete namespace masterbet-namespace")

    elif val == 11:
        print("Quel genre de test souhaitez-vous réaliser ?")
        print("1 - Test d'intégration.")
        print("2 - Test de charge.")
        input_kind_test = input('Quel est votre choix ?')
        run_test(int(input_kind_test))

def container_down(input_stack_down):
    """
    Cette focntion sert a détruire les container Docker qui ont été lancés par Lubri
    """
    if input_stack_down == 1:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile min down")
    elif input_stack_down == 2:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile runner down")
    elif input_stack_down == 3:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile runner_app down")
    elif input_stack_down == 4:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile sonar down")
    elif input_stack_down == 5:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile elk down")
    elif input_stack_down == 6:
        print("Bien... Gardes ! Executez les conteneurs !")
        os.system("docker compose --profile tig down")

def run_test(input_kind_test):
    """
    Fonction permettant de lancer les test via pytest & locust
    """
    if input_kind_test == 1:
        os.system("python -m pytest -rP", )
    if input_kind_test == 2:
        Popen('flask run --reload', creationflags=CREATE_NEW_CONSOLE)
        time.sleep(1)
        url_list = ["http://localhost:8089", "http://localhost:5000"]
        open_browser(url_list)
        os.system("cd ./test && locust")

def open_browser(url_list):
    """
    Fonction qui permet de déterminer si un navigateur est ouvert, dans ce cas, un onglet est ouvert plutot qu'une nouvelle fenêtre
    """
    for elem in url_list:
        print(elem)
        if webbrowser.get():
            webbrowser.open_new_tab(elem)
        else:
            webbrowser.open(elem)

def main():
    """
    Fonction main
    """
    while True:
        action(choice_func())

if __name__ == "__main__":
    main()
