# Les images DOCKERHUB

Les tags font référence aux variables utilisés dans la chaine de connection du fichier db.py se trouvant dans ./model/db.py

Le nom du host change en fonction de la stack utilisée.

26/09/2021 : La stack Kubernetes n'est pas fonctionnelle. Les pods ne s'inint pas avec les données du dump.sql quand on les lance. La connection a la db fonctionne, mais il faut trouver un truc pour que les pods mysql importent les données. Piste : réussir a initialiser le pod avec le dump.sql présent dans le docker-initdb.d a la manière du docker-compose.

Pour l'instant le ./model/db.py doit rester avec une chaine de connection en localhost pour nous permettre de travailler en local.

**Avant chaque push, changer cette chaine de connection pour que le runner pousse l'image adaptée**

## Docker-compose :

**Image 1** : dlefebvre/masterbet:mysql 

C'est l'image fonctionnelle pour le docker-compose.
Dans le fichier ./docker-compose, vérifier les lignes 19->22. La partie build doit être commentée, et l'image doit être pull depuis dockerhub.

## Kubernetes

**Image 2** : dlefebvre/masterbet:mysql-service

C'est l'image utilisé par kubernetes.


Name : Running Containers
Type : single number
Telegraf -W docker -> n_containers_running
base : fire
>= 5 Viridian
>=7 Planet

Name: Container CPU
TypeGraph
telegraf -> Docker_container_cpu -> usage_percent -> cocher tous les containes
Customize -> DD/MM/YYYY

Name : Container RAM usage
Telegraf -> Docker_container_mem -> Usage
Customize : positionning -> stacked 

Name : Swap
telegraf -> swap -> used

Name : CPU cores
Type : single stats
system -> n_cpus
base : fire
>=4 Viridian

## Lexique

Masters = Monnaie virtuelle gagnée et dépensée par les utilisateurs - Valeur unitaire (indivisible)

Utilisateur = Personne ayant un compte sur le site et pouvant participer aux paris mais aussi les créer

Auteur = Utilisateur ayant créé un pari

Participant = Utilisateur ayant mis en jeu des masters dans un pari

Administrateur = Utilisateur possédant les privilèges concernant la modération / édition des paris. Il a accès aux outils de développement.

Modérateur = Utilisateur disposant des privilèges de clôtures de paris à des fins de modération (Légal / Triche / Haine...)

Pari = Post contenant un titre, un corps de texte et la prédiction. Il peut avoir plusieurs états ainsi que des commentaires

En cours = Statut d'un pari non-résoluRésolution = Process par lequel un pari passe de l'état "non-résolu" à "résolu" Les gains sont réattribués.

Gagnant = Utilisateur ayant voté pour la prédiction gagnante

Perdant = Utilisateur ayant voté pour une des prédictions perdantes

Résolu = Etat d'un pari clôturé par son rédacteur / un administrateur / un modérateur

Mise initiale = Nombre de master mis en jeu par un utilisateur avant le processus de résolution. Elle lui sera restituée s'il remporte le pari.

Gain = Somme que l'utilisateur gagnant va récupérer du pot perdant

Gain total = Gain + mise initiale

Pot gagnant = Somme totale des masters engagés par les gagnants du pari. C'est la somme utilisée (au prorata) pour déterminer le gain final des utilisateurs gagnants.

Pot perdant = Somme totale des masters engagés par les perdants du pari que les gagnants vont se partager au prorata du montant de leur participation au pot gagnant

Pot total = Somme totale des masters engagés par tous les utilisateurs sur toutes les prédictions d'un pari donné.

Prédiction = Ensemble des options d'un pari à disposition des utilisateurs

Option = Issue potentielle d'un pari. Elle est créée par l'auteur dudit pari.

Vote = Choix d'une option + mise initiale d'un utilisateur sur un pari

Catégorie = Regroupement de paris d'un même thème (défini lors de sa création)

Groupe = Regroupement d'utilisateurs (public ou privé)

Public = Groupe acceptant automatiquement des demandes d'adhésion

Privé = Groupe où les membres acceptent ou non les nouvelles demandes d'adhésion

Commentaire = Texte qu'un utilisateur peut poster sous un pari

S'inscrire = enregistrer un compte utilisateur pour pouvoir utiliser le site


## LUBRI, le petit assistant

LUBRI est un script permettant d'effectuer la plupart des actions majeures pour l'application masterbet. Il permet, entre autre :
- De lancer l'application en local en précisant un port.
- De lancer l'application en version dockerisée, dans différentes configurations.
- De tuer les containers et nettoyers les networks.
- De Lancer la stack Kubernetes.
- D'executer certains tests.

## Installation des prérequis et des dépendances
**0) Pull du projet depuis GitLab**

- Créer un dossier 
````shell
mkdir masterbet
````
- Dans ce dossier, cloner le projet Masterbet
````shell
git clone https://gitlab.com/D-Shaman/masterbet.git
````

**1) Créer un venv et installer les dépendances :**

a)  Création de l'environnement virtuel

```shell
python3 -m venv venv
```

b) Activation de l'environnement virtuel

```shell
./venv/Script/activate
```

c) Installation des dépendances
```shell
pip install -r requirements.txt
```

d) Vérification des dépendances

```shell
pip list
```

## Lancer l'application en local

Vous pouvez utiliser Lubri, ou suivre la procédure suivante :

1) Activation de l'environnement virtuel
```shell
./venv/Script/activate
```
2) Lancement de l'application
```shell
flask run --reload
```
3) Accéder a l'application dans le navigateur
```shell
http://localhost:5000
```
4) Lancer l'application sur un autre port
```shell
flask run --reload -p<port>
```
5) Se connecter a l'application
```shell
http://localhost:<port>
```

## Lancement de l'aplication via docker

L'application et ses dépendances sont pensées pour être lancées via Docker.

0) Installation de docker et docker-compose

Nécessite : docker, docker-compose

https://www.docker.com/products/docker-desktop

Quand le docker compose est utilisé, l'image adaptée est toujours tag de la manière suivante : dlefebvre/masterbet:mysql_*

La version dlefebvre/masterbet:mysql-service_* est une version de l'image utilisée pour Kubernetes.

Page DockerHub de l'application Masterbet :

```shell
https://hub.docker.com/repository/docker/dlefebvre/masterbet
```

1) Lancement de l'application en configuration minimale via docker-compose

Dans le dossier */masterbet/

```shell
docker compose --profile min up -d
```
Le tag -f permet de préciser le fichier de configuration a utiliser. Le tag -p permet de préciser un nom de projet. Ces deux tags sont nécessaire au lancement des containers.

2) Fermeture de l'application :

```shell
docker compose --profile min down
```
3) Lancement de l'application hors configuration minimale

S'il est nécessaire de l'ancer l'application hors configuration minimale, il est possible de commenter les services inutiles dans le docker-compose.yml et de lancer les containers via la commande

```shell
docker compose up -d
```

Les services apparaissant sur le configuration minimale sont cependant nécessaire : mysql, masterbet_app. Si sonarcube est lancé, sa sb est nécessaire.

## Code quality : Sonarqube

Le projet contient une stack sonarqube qu'il est possible d'exploiter dans le pipeline gitlab-ci.

1) Lancement du compose
```shell
docker compose --profil sonar up -d
```
2) Connection a l'interface graphique de Sonarqube
Dans un navigateur : 
```shell
http://localhost:7000
```

Login

Login : admin; password : admin

Token name : masterbet

Copier le token vers la ligne :

```shell
sonar-scanner -Dsonar.projectKey=mast -Dsonar.sources=. -Dsonar.host.url=http://172.20.0.30:9000 -Dsonar.login=<token>
```
3) Push le projet
```shell
git add. 
git commit -m "test sonar"
git push
```
4) Voir le résultat

A la fin du pipeline, retourner sur 
```shell
http://localhost:7000
```

## Logs de l'application : suite ELK

Masterbet utilise la suite ELK pour récupérer les logs d'utilisation de l'application.

Cette méthode utilise un server nginx pour monitorer l'application via proxy. Logstash s'occupe des logs et envoie les données a kibana/elastic.

Troobleshoot : vérifier que les lignes du port dans le service masterbet_app du docker compose sont commentées.

1) Lancer la stack ELK

Vous pouvez utiliser Lubri, il se chargera pour vous de lancer l'intégralité des composantes nécessaire au fonctionnement de la suite.

Ou, vous pouvez utiliser :

```shell
docker compose --profile elk up -d
```

Dans cette configuration, pour accéder a l'application, il faut s'addresser au serveur nginx :

```shell
http://localhost:80
```

2) Connectez vous a kibana

```shell
http://localhost:5601
```

Les information de connection sont les suivantes : 
- Login : elastic
- Mpd : changeme

Une fois connecté a l'outil, les logs de l'application peuvent être explorés dans :

management -> stack management 
kibana -> index pattern
analytics -> Dashboard

3) Fermer les conteneurs :
```shell
docker compose --profile elk down
``` 

## Monitoring de l'application, suite TIG

Masterbet utilise la suite TIG pour monitorer l'application et s'assurer de l'état de santé des containers. Plus spécifiquement, de l'interface d'influxdb avec les information fournies par telegraf.

1) Lancer la stack TIG

Vous pouvez utiliser lubri pour lancer la stack TIG.

Ou utiliser la commande suivante : 

```shell
docker compose --profile tig up -d
```

2) Se rendre sur l'interface web

Pour vous rendre sur l'interface web, ouvrez un navigateur:

```shell
http://localhost:8086
```
Les informations suivantes sont a renseigner :

Nom d'utilisateur : telegraf_user
Mot de passe : telegraf_password
Nom initial de l'organisation : masterbet
Nom initial du compartiment : telegraf

3) Changement du token

Dans données -> Jeton -> Jeton de telegraf_user -> Copier dans le presse papier

Dans */masterbet/telegraf/telegraf.conf, mettre le nouveau token (ligne ~846).

Redemarrer le service telegraf, recharger la page web.

L'utilisateur peut créer un nouveau dashboard.

4) Fermeture des conteneurs
```shell
docker-compose --profile elk down
```
## Utilisation des runners pour gitlab-ci

Deux runners son configurés pour accomplir le pipeline gitlab-ci quand un commit est effectué. Ce pipeline comprend différentes étapes, mais dans tous les cas, les runners doivent être lancés.

LES RUNNERS DOIVENT ETRE LANCES AVANT CHAQUE PUSH

1) Lancer les runners

Vous pouvez utiliser lubri pour lancer les runners. Il est pratique Lubri quand même...

Sinon, vous pouvez utiliser la commande:

```shell
    docker compose --profile runner up -d
```
Ou, si vous souhaitez lancer l'application en même temps que les runners : 

```shell
docker compose --profile runner_app up -d
```

## Test des routes : Pytest

BN : les tests peuvent être lancés via Lubri

1) Vérifier que pytest fait bien parti de la liste des modules installés dans le venv.

```shell
pip list
```
Pytest doit apparaitre dans la liste des modules installés dans le venv.

2) Dans le directory masterbet

Lancer Pytest avec la verbose.
```shell
py -m pytest -rP
```

Lancer pytest sans la verbose.

```shell
py -m pytest
```

Générer des logs des tests effectués :
```shell
python -m pytest -rP > ./test/testlogs.log
python -m pytest -rP --junitxml=./test/report.xml
```

Pytest va scanner le dossier ./test pour trouver les fichiers conftest.py et test_1.py contenant les configuration et les tests.

Il est a noté que pytest est utilisé dans le fichier de configuration du pipeline CI/CD de GitLab. Voir .gitlab-ci.yml.

## Test de la charge : Locust

Locust n'est pas inclus dans le pipeline Gitlab-Ci, il faut effectuer les test en local.

1) Vérifier que locust fait bien parti de la liste des modules installés dans le venv.

```shell
pip list
```

2) Lancer l'application

Dans un premier terminal, lancez l'application. Référez-vous a la section : lancer l'application pour lire la procédure. Ouvrez un autre terminal pour lancer locust.

3) Lancer locust en interface web

a) Se placer dans le dossier test.

Depuis le dossier */masterbet/
```shell
cd ./test
```
b) Lancer locust

```shell
locust
```
c) Se connecter a l'interface graphique de locust

Dans un navigateur, connectez vous en localhost sur le port 8089.
````shell
http://localhost:8089
````

4) Lancer locust en headless

a) Se placer dans le dossier test.

Depuis le dossier */masterbet/
```shell
cd ./test
```
b) Lancer locust
```shell
locust -f .\locustfile.py --headless -u 15 -t 15 --host http://localhost:5000
```
Le flag -u peut être modifier pour changer le nombre de connection a l'hôte. Le flag -t peut être modifié pour changer le timer du test.

c) Générer un fichier de log de locust

````shell
locust -f .\locustfile.py --headless -u 15 -t 15 --host http://localhost:5000 --logfile=./test.locustlog.log
````

Le fichier de logs sera généré dans /masterbet/test/test.locustlog.log


Accéder a l'application en cloud :

http://34.78.194.219/